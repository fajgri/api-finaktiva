package com.finaktiva.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.finaktiva.api.entity.Rol;

import java.io.Serializable;
import java.math.BigDecimal;

public class UsuarioDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("nombre")
    private String nombre;

    @JsonProperty("apellido")
    private String apellido;

    @JsonProperty("correo")
    private String correo;


    @JsonProperty("pais")
    private String pais;

    @JsonProperty("ciudad")
    private String ciudad;

    @JsonProperty("contrasena")
    private String contrasena;

    @JsonProperty("telefono")
    private String telefono;

    private Rol rol;


    public UsuarioDto(Long id, String nombre, String apellido, String correo, String pais, String ciudad, String contrasena, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.pais = pais;
        this.ciudad = ciudad;
        this.contrasena = contrasena;
        this.telefono = telefono;
    }

}