package com.finaktiva.api.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


    @Entity
    @Table(name = "usuario",schema = "public")
    public class Usuario implements Identifiable, Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Long id;

        @Column(name = "nombre", length = 50, nullable = false)
        private String nombre;

        @Column(name = "apellido", length = 300, nullable = false)
        private String apellido;

        @Column(name = "correo", length = 300, nullable = false)
        private String correo;

        @Column(name = "pais", length = 300, nullable = false)
        private String pais;

        @Column(name = "ciudad", length = 300, nullable = false)
        private String ciudad;

        @Column(name = "contrasena", length = 300, nullable = false)
        private String contrasena;

        @Column(name = "telefono", length = 300, nullable = false)
        private String telefono;

        @ManyToOne
        @JoinColumn(name = "rol_id", nullable = false)
        private Rol rol;

        public Usuario() {}

        @Override
        public Long getId() {
            return this.id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public String getCorreo() {
            return correo;
        }

        public void setCorreo(String correo) {
            this.correo = correo;
        }

        public String getPais() {
            return pais;
        }

        public void setPais(String pais) {
            this.pais = pais;
        }

        public String getCiudad() {
            return ciudad;
        }

        public void setCiudad(String ciudad) {
            this.ciudad = ciudad;
        }

        public String getContrasena() {
            return contrasena;
        }

        public void setContrasena(String contrasena) {
            this.contrasena = contrasena;
        }

        public String getTelefono() {
            return telefono;
        }

        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }

        public Rol getRol() {
            return rol;
        }

        public void setRol(Rol rol) {
            this.rol = rol;
        }
    }
