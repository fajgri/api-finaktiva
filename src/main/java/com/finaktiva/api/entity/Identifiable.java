package com.finaktiva.api.entity;

public interface Identifiable {

    public Long getId();
}
