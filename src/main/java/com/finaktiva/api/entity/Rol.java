package com.finaktiva.api.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name = "rol", schema = "public")
public class Rol implements   Identifiable, Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "nombre", length = 50, nullable = false)
    private String nombre;

    @Column(name = "descripcion", length = 300, nullable = false)
    private String description;


    public void setId(Long id) { this.id = id; }

    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getDescription() {return description; }

    public void setDescription(String description) {this.description = description; }

    @Override
    public Long getId() {
        return this.id;
    }
}
