package com.finaktiva.api.dao;

import com.finaktiva.api.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface UsuarioDao  extends PagingAndSortingRepository<Usuario, Long> {

    @Query("SELECT  this FROM Usuario this WHERE  this.rol.id = :idRol")
    public Page<Usuario> searchByRolId(Pageable pegeable, @Param("idRol") Long idRol);

}
