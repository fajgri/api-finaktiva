package com.finaktiva.api.dao;

import com.finaktiva.api.entity.Rol;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolDao extends PagingAndSortingRepository<Rol, Long> {

}
