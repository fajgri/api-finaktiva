package com.finaktiva.api.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends  RuntimeException {
    public  ResourceNotFoundException(){
        super();
    }

    public ResourceNotFoundException(String arg,Throwable throwable){
        super(arg,throwable);
    }

    public ResourceNotFoundException(String arg){
        super(arg);
    }

}
