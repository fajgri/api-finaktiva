package com.finaktiva.api.controller;

import com.finaktiva.api.dao.RolDao;
import com.finaktiva.api.dto.RolDto;
import com.finaktiva.api.entity.Rol;
import com.finaktiva.api.error.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;


import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

@RestController("rolController")
public class RolController {

   @Autowired
    private RolDao rolDao;

    @GetMapping(value = "/api/Rol")
    public ResponseEntity<Page<RolDto>> getRol(Pageable pageable){
        Page<RolDto> result = rolDao.findAll(pageable).map( new RolToUsuarioDto());
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/api/Rol/{id}")
    public ResponseEntity<RolDto> getRol(@PathVariable Long id){
        Optional<RolDto> result = rolDao.findById(id).map(new RolToUsuarioDto());
        return  new ResponseEntity<>(result.get(), HttpStatus.OK);

    }

    @PostMapping(value = "/api/Rol")
    public ResponseEntity<?> newRol(@RequestBody Rol rol) {
        rol.setId(null);
        rol =  rolDao.save(rol);

        HttpHeaders responseHeaders = new HttpHeaders();
        URI newURI = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(rol.getId())
                .toUri();
        responseHeaders.setLocation(newURI);

        return  new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
    }


    @PutMapping(value = "/api/Rol/{id}")
    public  ResponseEntity<?> updateRol(@RequestBody Rol rol, @PathVariable Long id){
        verifyRol(id);
        rol.setId(id);

        rol = rolDao.save(rol);
        return  new ResponseEntity<>(HttpStatus.OK);
    }

    private  Rol verifyRol(Long id) throws ResourceNotFoundException {
        Optional<Rol> rol = rolDao.findById(id);
        if(!rol.isPresent()){
            throw  new  ResourceNotFoundException(String.format("Rol %d not found"));

        }
        return  rol.get();
    }

    @DeleteMapping(value = "/api/Rol/{id}")
    public  ResponseEntity<?> deleteRol(@RequestBody Rol rol, @PathVariable Long id){
        verifyRol(id);
        rol.setId(id);

        rolDao.deleteById(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }

    private class RolToUsuarioDto implements Function<Rol, RolDto> {

        public RolDto apply(Rol rol){
            return  new RolDto(
                    rol.getId(),
                    rol.getNombre(),
                    rol.getDescription()

            );
        }
    }


}
