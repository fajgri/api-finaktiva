package com.finaktiva.api.controller;

import com.finaktiva.api.dao.UsuarioDao;
import com.finaktiva.api.dto.UsuarioDto;
import com.finaktiva.api.entity.Usuario;
import com.finaktiva.api.error.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

@RestController("usuarioController")
public class UsuarioController {
    @Autowired
    private UsuarioDao usuarioDao;

    @GetMapping(value = "/api/Usuario")
    public ResponseEntity<Page<UsuarioDto>> getUsuario(Pageable pageable) {
        Page<UsuarioDto> result = usuarioDao.findAll(pageable).map(new UsuarioToUsuarioDto());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/api/Usuario/{id}")
    public ResponseEntity<UsuarioDto> getUsuario(@PathVariable Long id) {
        Optional<UsuarioDto> result = usuarioDao.findById(id).map(new UsuarioToUsuarioDto());
        return new ResponseEntity<>(result.get(), HttpStatus.OK);
    }

    @GetMapping(value = "/api/Usuario/GetUsuarioByRol")
    public ResponseEntity<Page<UsuarioDto>> getUsuarioByRol(Pageable pageable, @RequestParam("idRol") Long idRol) {
        Page<UsuarioDto> result = usuarioDao.searchByRolId(pageable, idRol).map(new UsuarioToUsuarioDto());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = "/api/Usuario")
    public ResponseEntity<?> newUsuario(@RequestBody Usuario usuario) {
        usuario.setId(null);
        usuario = usuarioDao.save(usuario);


        HttpHeaders responseHeaders = new HttpHeaders();
        URI newURI = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(usuario.getId())
                .toUri();
        responseHeaders.setLocation(newURI);

        return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
    }

    @PutMapping(value = "/api/Usuario/{id}")
    public ResponseEntity<?> updateUsuario(@RequestBody Usuario usuario,  @PathVariable Long id) {
        verifyUsuario(id);
        usuario.setId(id);

        usuario = usuarioDao.save(usuario);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/Usuario/{id}")
    public ResponseEntity<?> deleteUsuario(@RequestBody Usuario usuario,  @PathVariable Long id) {
        verifyUsuario(id);
        usuario.setId(id);

        usuarioDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private Usuario verifyUsuario(Long id) throws ResourceNotFoundException {
        Optional<Usuario> usuario = usuarioDao.findById(id);

        if (!usuario.isPresent()) {
            throw new ResourceNotFoundException(String.format("Product %d not found", id));
        }

        return usuario.get();
    }

    private class UsuarioToUsuarioDto implements Function<Usuario, UsuarioDto> {

        public UsuarioDto apply(Usuario usuario) {
            return new UsuarioDto(
                    usuario.getId(),
                    usuario.getNombre(),
                    usuario.getApellido(),
                    usuario.getPais(),
                    usuario.getCiudad(),
                    usuario.getCorreo(),
                    usuario.getContrasena(),
                    usuario.getTelefono()

            );
        }
    }
}